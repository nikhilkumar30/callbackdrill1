const fs_problem1 = require('../fs-problem1');
const path = require('path');

const dirpath = path.join(__dirname, 'test');
const numberOfFiles = 3;

try {
  const createFiles = fs_problem1.createFiles(dirpath, numberOfFiles);
  console.log('Files created:', createFiles);

  try {
    const delFiles = fs_problem1.deleteFiles(createFiles);
    console.log(delFiles);
  } catch (delErr) {
    console.log('Error deleting files:', delErr.message);
  }
} catch (createErr) {
  console.log('Error creating files:', createErr.message);
}
