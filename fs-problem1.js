const path = require("path");
const fs = require("fs");

function createFiles(dirpath, numberOffiles) {
  try {
    fs.mkdirSync(dirpath);
    const createFiles = [];
    for (let index = 1; index <= numberOffiles; index++) {
      const fileName = "file${index}.json";
      const filePath = path.join(dirpath, fileName);
      const randomData = { value: Math.random() };

      fs.writeFileSync(filePath, JSON.stringify(randomData));
      createFiles.push(filePath);
    }
    return createFiles;
  } catch (err) {
    throw new Error(`Error in files: ${err.message}`)
  }
}

function deleteFiles(filePath) {
  try {
    filePath.forEach((filePath) => {
      fs.unlinkSync(filePath);
    });
    return "files deleted successfully";
  } catch (err) {
    throw new Error(`Error deleting in files: ${err.message}`);
  }
}

module.exports = { createFiles, deleteFiles };
